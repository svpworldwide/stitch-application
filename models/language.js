const mongoose = require('mongoose');

const languageSchema2 = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true, index: true },
    nativeName: { type: String, trim: true },
  },
  { timestamps: true }
);

const Language2 = mongoose.model('Language2', languageSchema2, 'languages2');

exports.languageSchema2 = languageSchema2;
exports.Language2 = Language2;
