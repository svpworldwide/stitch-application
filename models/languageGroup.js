const mongoose = require('mongoose');

const languageGroupSchema2 = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true },
    languages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Language2' }],
  },
  { timestamps: true }
);

const LanguageGroup2 = mongoose.model('LanguageGroup2', languageGroupSchema2, 'languagegroups2');

exports.LanguageGroup2 = LanguageGroup2;
