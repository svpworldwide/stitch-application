const passport = require('passport');
const { v4: uuidv4 } = require('uuid');
const AWS = require('aws-sdk');
const { isAuthorized } = require('./authMiddleware');
const createLog = require('../utils/createLog');
const User = require('../models/user');

AWS.config.update({
  accessKeyId: process.env.AWS_ACCES_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCES_KEY,
  region: process.env.AWS_REGION,
});

const passwordRegex = /^(?=.*\d)(?=.*[!@#$%^&*(){}[\]])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{7,}$/g;

module.exports = (app) => {
  // Login route
  app.post('/api/login', passport.authenticate('local'), async (req, res) => {
    if (!req.user) return res.status(401).json({ loggedIn: false });

    await createLog(req, 'log_in');

    return res.status(200).json({
      loggedIn: true,
      username: req.user.username,
      email: req.user.email,
      role: req.user.role,
      expires: req.session.cookie.expires,
    });
  });

  // Logout route
  app.get('/api/logout', async (req, res) => {
    await createLog(req, 'log_out');

    req.logout();

    return res.json({ message: 'Logged out' });
  });

  // Register route
  app.post('/api/register', isAuthorized(['superadmin']), async (req, res) => {
    const ses = new AWS.SES({ apiVersion: '2010-12-01' });

    const token = uuidv4();

    const userData = {
      role: 'user',
      ...req.body,
      status: 'verification',
      token,
    };

    await new User(userData).save();

    const url = process.env.NODE_ENV === 'production' ? process.env.PROD_URL : process.env.DEV_URL;
    const registrationLink = `${url}/account-verification/${token}`;
    let name;
    if (userData.username) name = userData.username;
    if (userData.firstName) name = userData.firstName;

    const params = {
      Destination: {
        ToAddresses: [userData.email], // Email address/addresses that you want to send your email
      },
      Message: {
        Body: {
          Html: {
            // HTML Format of the email
            Charset: 'UTF-8',
            Data: `
              <html>
                <body>
                  <h3>Hello${name ? ' ' + name : ''},</h3>
                  <p>You have been registered to Singer Stitch App.</p>
                  <p>Please click the link below to complete registration process:</p>
                  <a href="${registrationLink}">${registrationLink}</a>
                </body>
              </html>
            `,
          },
          Text: {
            Charset: 'UTF-8',
            Data: `Hello${
              name ? ' ' + name : ''
            },\nYou have been registered to Singer Stitch App.\nPlease go to the link below to complete registration process:\n${registrationLink}`,
          },
        },
        Subject: {
          Charset: 'UTF-8',
          Data: 'Registration',
        },
      },
      Source: process.env.EMAIL_FROM,
    };

    const sendEmail = ses.sendEmail(params).promise();

    sendEmail
      .then(async (data) => {
        console.log('\n Email submitted to SES', data);

        await createLog(req, 'register', { user: req.body.email });

        return res.json({ message: 'registered' });
      })
      .catch((error) => {
        console.log('\n Error submitting email to SES :: ', error);

        return res.status(500).json({ message: error.message });
      });
  });

  // Account verification route
  app.post('/api/account-verification/:token', async (req, res) => {
    if (!req.body.password.match(passwordRegex)) {
      return res.status(500).json({
        message:
          'Password should be at least 7 characters long, with at least 1 uppercase letter, 1 number and 1 special character',
      });
    }

    const user = await User.findOne({ email: req.body.email });

    if (!user) {
      return res.status(500).json({ message: 'User not found' });
    }

    if (user && user.token.toString() !== req.params.token.toString()) {
      return res.status(500).json({ message: 'Invalid token' });
    }

    await User.findOneAndUpdate(
      { email: req.body.email },
      { token: null, status: 'active', password: req.body.password },
      { useFindAndModify: false }
    );

    await createLog(req, 'account_verification', { user: req.body.email });

    return res.json({ message: 'Account verified' });
  });

  // Forgot password
  app.post('/api/forgot-password', async (req, res) => {
    const { email } = req.body;
    const ses = new AWS.SES({ apiVersion: '2010-12-01' });

    const token = uuidv4();

    await User.findOneAndUpdate({ email }, { $set: { token } }, { useFindAndModify: false });

    const url = process.env.NODE_ENV === 'production' ? process.env.PROD_URL : process.env.DEV_URL;
    const resetPasswordLink = `${url}/reset-password/${token}`;

    const params = {
      Destination: {
        ToAddresses: [email], // Email address/addresses that you want to send your email
      },
      Message: {
        Body: {
          Html: {
            // HTML Format of the email
            Charset: 'UTF-8',
            Data: `
              <html>
                <body>
                  <h3>Hello,</h3>
                  <p>You requested password reset for Your Singer Stitch App account,</p>
                  <p>please click the link below and enter Your new password:</p>
                  <a href="${resetPasswordLink}">${resetPasswordLink}</a>
                </body>
              </html>
            `,
          },
          Text: {
            Charset: 'UTF-8',
            Data: `Hello,\nYou requested password reset for Your Singer Stitch App account,\nplease click the link below and enter Your new password:\n${resetPasswordLink}`,
          },
        },
        Subject: {
          Charset: 'UTF-8',
          Data: 'Forgot password',
        },
      },
      Source: process.env.EMAIL_FROM,
    };

    const sendEmail = ses.sendEmail(params).promise();

    sendEmail
      .then(async (data) => {
        console.log('\n Email submitted to SES', data);

        await createLog(req, 'forgot_password', { user: email });

        return res.json({ message: 'Email sent' });
      })
      .catch((error) => {
        console.log('\n Error submitting email to SES :: ', error);

        return res.status(500).json({ message: error.message });
      });
  });

  // Reset password
  app.post('/api/reset-password/:token', async (req, res) => {
    if (!req.params.token) {
      return res.status(500).json({ message: 'Missing reset password token!' });
    }

    if (!req.body.password.match(passwordRegex)) {
      return res.status(500).json({
        message:
          'Password should be at least 7 characters long, with at least 1 uppercase letter, 1 number and 1 special character!',
      });
    }

    const user = await User.findOne({ email: req.body.email });

    if (!user) {
      return res.status(500).json({ message: 'User not found!' });
    }

    if (user && (!user.token || user.token !== req.params.token)) {
      return res.status(500).json({ message: 'Invalid user email!' });
    }

    await User.findOneAndUpdate(
      { email: req.body.email },
      { token: null, password: req.body.password },
      { useFindAndModify: false }
    );

    await createLog(req, 'reset_password', { user: req.body.email });

    return res.json({ message: 'New password set' });
  });
};
