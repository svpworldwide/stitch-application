const { isAuthenticated } = require('./authMiddleware');
const createLog = require('../utils/createLog');
const { Translation } = require('../models/translation');
const { Language2 } = require('../models/language');
const { Stitch2 } = require('../models/stitch');
const { LanguageGroup2 } = require('../models/languageGroup');
const { Pdf } = require('../models/pdf');

module.exports = (app) => {
  app.get('/api/v2/translations', isAuthenticated, async (req, res) => {
    const query = {};
    const { language } = req.query;
    if (language) query.language = language;
    try {
      const data = await Translation.find(query)
        .populate('stitch', 'appNumber image index')
        .populate('language', 'name')
        .sort({ 'stitch.index': 1 })
        .exec();
      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.post('/api/v2/translations', isAuthenticated, async (req, res) => {
    try {
      const data = await new Translation(req.body).save();

      const lang = await Language2.findOne({ _id: req.body.language }).lean();

      const stitch = await Stitch2.findOne({ _id: req.body.stitch }).lean();

      await createLog(req, 'create_translation', {
        machine: req.body.number,
        stitch: stitch ? stitch.appNumber : req.body.stitch,
        language: lang ? lang.name : req.body.language,
      });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.put('/api/v2/translations/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const languageGroups = await LanguageGroup2.find({ languages: req.body.language }).lean();
      const languageGroupIds = languageGroups.map((gr) => gr._id);

      const data = await Translation.findOneAndUpdate({ _id: id }, req.body, { useFindAndModify: false });

      await Pdf.updateMany(
        {
          $or: [{ language: req.body.language }, { languageGroup: { $in: languageGroupIds } }],
        },
        { $set: { newVersion: true } }
      );

      const lang = await Language2.findOne({ _id: req.body.language }).lean();

      const stitch = await Stitch2.findOne({ _id: req.body.stitch }).lean();

      await createLog(req, 'update_translation', {
        machine: req.body.number,
        stitch: stitch ? stitch.appNumber : req.body.stitch,
        language: lang ? lang.name : req.body.language,
      });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.delete('/api/v2/translations/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      // *** Delete pdf files from disk --- NOT USED
      // const files = await Pdf.find({ language: id }).exec();
      // for (const { _id, file } of files) {
      //   const filePath = path.resolve('public', 'pdf', file.replace('/pdf/', ''));
      //   if (fs.existsSync(filePath)) {
      //     fs.unlinkSync(filePath);
      //   }
      //   await Pdf.findOneAndDelete({ _id });
      // }

      const data = await Translation.findOneAndDelete({ _id: id });

      const lang = await Language2.findOne({ _id: data.language }).lean();

      const stitch = await Stitch2.findOne({ _id: data.stitch }).lean();

      await createLog(req, 'delete_translation', {
        machine: req.body.number,
        stitch: stitch ? stitch.appNumber : req.body.stitch,
        language: lang ? lang.name : req.body.language,
      });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });
};
