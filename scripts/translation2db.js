const { readFile, utils } = require('xlsx');
const mongoose = require('mongoose');
const Log = require('../models/log').Log;
const { Translation } = require('../models/translation');
const { Language2 } = require('../models/language');
const { Stitch2 } = require('../models/stitch');
require('dotenv').config();

const arg = process.argv[2];
let workbook;
if ([undefined, '', ' ', '?', '-?', '-h', '--help'].includes(arg)) {
  console.log('\nUsage: node xlsx2db.js <path-to-file>');
  console.log('\nExample: node xlsx2db.js ..doc/Stitch_Applications_Sewing_Worksheet_Translations_LINKS.xlsx\n');
  return;
} else {
  try {
    workbook = readFile(arg);
  } catch (e) {
    console.log(`\n${arg} is not a valid xlsx file!\n`);
    return;
  }
}

mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('\nDatabase connected...\n');

    await Translation.deleteMany({}).exec();

    const save = await workbook2db();

    console.log('\nResult: ', save);
  })
  .catch(async (e) => {
    console.log('\nError saving to database!');
    console.log(e.message);
    console.log('\nResult: ', 'NOT SAVED');
    await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'error_save_2db',
    }).save();
  })
  .finally(() => {
    mongoose.disconnect();
    console.log('\nDatabase disconnected\n');
  });

const sheet2json = async (sheet, sheetName, machinesDataRange) => {
  const language = await Language2.findOne({ name: sheetName }).exec();
  let json = { languageName: language.name, language: language._id, translations: [] };
  sheet['!ref'] = machinesDataRange;
  const range = utils.decode_range(sheet['!ref']);
  for (row = range.s.r; row <= range.e.r; row++) {
    let translation = {};
    const index = row - 4;
    const appNumberCell = sheet[utils.encode_cell({ r: row, c: 0 })]; // col A is app number
    const appNumber = appNumberCell && appNumberCell.w ? appNumberCell.w : '';
    let stitch = await Stitch2.findOne({ index, appNumber }).exec();
    if (!stitch) {
      const image = sheet[utils.encode_cell({ r: row, c: 2 })]; // col C is image
      stitch = await new Stitch2({ index, appNumber, image: image.w }).save();
    }
    translation.language = language._id;
    translation.stitch = stitch._id;
    const name = sheet[utils.encode_cell({ r: row, c: 1 })]; // col B is name
    translation.name = name && name.w ? name.w : '';
    const application = sheet[utils.encode_cell({ r: row, c: 3 })]; // col D is application
    translation.application = application && application.w ? application.w : '';
    const description = sheet[utils.encode_cell({ r: row, c: 5 })]; // col F is description
    translation.description = description && description.w ? description.w : 'No description avaialble';
    json.translations.push(translation); // save translation to array
  }
  return json;
};

const json2db = async (json) => {
  try {
    const { language, translations } = json;
    console.log({ language, translations: translations.length });
    for (const item of translations) {
      await new Translation({ language, ...item }).save();
    }
  } catch (e) {
    console.log('\nError converting to json!\n');
    console.log(e);
  }
};

const workbook2db = async () => {
  let result;
  for (let sheet of workbook.SheetNames) {
    const worksheet = workbook.Sheets[sheet];
    const json = await sheet2json(worksheet, sheet, 'O5:BM785'); // machine data range

    result = await json2db(json)
      .then(() => {
        return 'SAVED';
      })
      .catch((e) => {
        console.log('\nNOT saved!');
        console.log(e);
        return 'NOT SAVED';
      });
  }
  return result || '\nFinished!';
};
