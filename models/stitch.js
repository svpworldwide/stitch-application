const mongoose = require('mongoose');

const stitchSchema2 = new mongoose.Schema(
  {
    index: { type: Number, required: true },
    appNumber: { type: String, required: true, trim: true, index: true },
    image: { type: String, trim: true },
  },
  { timestamps: true }
);

const Stitch2 = mongoose.model('Stitch2', stitchSchema2, 'stitches');

exports.Stitch2 = Stitch2;
