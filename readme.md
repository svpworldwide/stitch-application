# Stitch application

## Prerequisites

Nodejs v12 or above  
Mongodb v5 or above

## Install

In project root folder run `npm i`  
Copy `.env.example` to new file `.env` and change AWS secrets

## Development start

In project root folder run `npm run dev`

## Deploy

You need `pm2` installed globally  
Ssh server access keys should be in project root `key` folder  
Deploy configuration is in pm2 config file in project root folder `ecosystem.config.js`  
Run `npm run deploy` to deploy to staging server  
Run `npm run deploy:production` to deploy to staging server
