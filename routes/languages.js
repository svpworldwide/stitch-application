const fs = require('fs');
const path = require('path');
const { ObjectId } = require('mongoose').Types;
const { isAuthenticated } = require('./authMiddleware');
const createLog = require('../utils/createLog');
const { Language2 } = require('../models/language');
const { Translation } = require('../models/translation');
const { LanguageGroup2 } = require('../models/languageGroup');
const { Pdf } = require('../models/pdf');

module.exports = (app) => {
  app.get('/api/v2/languages', isAuthenticated, async (req, res) => {
    try {
      const data = await Language2.find({}).exec();
      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.post('/api/v2/languages', isAuthenticated, async (req, res) => {
    try {
      const data = await new Language2(req.body).save();

      await createLog(req, 'create_language', { language: req.body.name });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.put('/api/v2/languages/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const data = await Language2.findOneAndUpdate({ _id: id }, req.body, { useFindAndModify: false });

      await createLog(req, 'update_language', { language: req.body.name });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.delete('/api/v2/languages/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const files = await Pdf.find({ language: id }).exec();
      for (const { _id, file } of files) {
        const filePath = path.resolve('public', 'pdf', file.replace('/pdf/', ''));
        if (fs.existsSync(filePath)) {
          fs.unlinkSync(filePath);
        }
        await Pdf.findOneAndDelete({ _id });
      }

      await LanguageGroup2.updateMany({ languages: ObjectId(id) }, { $pull: { languages: ObjectId(id) } });
      await Translation.deleteMany({ language: ObjectId(id) });

      const data = await Language2.findOneAndDelete({ _id: id });

      await createLog(req, 'delete_language', { language: data.name });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.get('/api/v2/language-groups', isAuthenticated, async (req, res) => {
    try {
      const data = await LanguageGroup2.find({}).sort({ name: 1 }).populate('languages').exec();
      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.post('/api/v2/language-groups', isAuthenticated, async (req, res) => {
    try {
      const data = await new LanguageGroup2(req.body).save();

      await createLog(req, 'create_language_group', { languageGroup: req.body.name });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.put('/api/v2/language-groups/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const data = await LanguageGroup2.findOneAndUpdate({ _id: id }, req.body, { useFindAndModify: false });

      await createLog(req, 'update_language_group', { languageGroup: req.body.name });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.delete('/api/v2/language-groups/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const files = await Pdf.find({ languageGroup: id }).exec();
      for (const { _id, file } of files) {
        const filePath = path.resolve('public', 'pdf', file.replace('/pdf/', ''));
        if (fs.existsSync(filePath)) {
          fs.unlinkSync(filePath);
        }
        await Pdf.findOneAndDelete({ _id });
      }

      const data = await LanguageGroup2.findOneAndDelete({ _id: id });

      await createLog(req, 'delete_language_group', { languageGroup: data.name });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });
};
