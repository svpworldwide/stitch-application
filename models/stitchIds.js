const mongoose = require('mongoose');

const idSchema = new mongoose.Schema({
  appNumber: { type: String, required: true, trim: true },
  index: { type: Number, required: true },
});

const stitchIdSchema = new mongoose.Schema({ stitchIds: [idSchema] }, { timestamps: true });

const StitchId = mongoose.model('StitchId', stitchIdSchema);

exports.stitchIdsSchema = stitchIdSchema;
exports.StitchId = StitchId;
