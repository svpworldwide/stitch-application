const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

// GENERAL
require('dotenv').config();

const PORT = process.env.PORT || 3400;

const app = express();

app.use(
  cors({
    credentials: true,
    allowedHeaders: 'Content-Type',
    methods: 'GET,POST,PUT,DELETE',
    origin: process.env.NODE_ENV === 'production' ? process.env.PROD_URL : 'http://localhost:3000',
  })
);

app.use(bodyParser.json());

// DATABASE
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

// AUTH SESSION
app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 1000 * 60 * 60 * 8, secure: false }, // 8 hours
  })
);

require('./config/passport');
app.use(passport.initialize());
app.use(passport.session());

// ROUTES
app.disable('x-powered-by');

app.get('/api/hello', (req, res) => res.json({ message: 'Hello world!' }));

require('./routes/auth')(app);
require('./routes/languages')(app);
require('./routes/stitches')(app);
require('./routes/machine')(app);
require('./routes/user')(app);
require('./routes/pdf')(app);
require('./routes/translation')(app);

// STATIC FILES
app.use(express.static('public'));

// ERROR HANDLER
app.use((err, req, res, next) => {
  if (err) {
    console.log(err);
    return res.json({ err });
  }
});

// SERVER START
app.listen(PORT, () => {
  console.log(`app running on port ${PORT}`);
});
