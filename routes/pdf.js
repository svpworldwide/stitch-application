const path = require('path');
const fs = require('fs');
const { ObjectId } = require('mongoose').Types;
const { isAuthenticated } = require('./authMiddleware');
const pdfgenerate = require('../utils/pdfgenerate');
const multiPDFGenerate = require('../utils/multiPDFGenerate');
const createLog = require('../utils/createLog');
const { Machine2 } = require('../models/machine');
const { Language2 } = require('../models/language');
const { LanguageGroup2 } = require('../models/languageGroup');
const { Pdf } = require('../models/pdf');

const getMachine = async (machine, language) => {
  try {
    const [data] = await Machine2.aggregate([
      { $match: { _id: ObjectId(machine) } },
      {
        $lookup: {
          from: 'stitches',
          localField: 'stitches',
          foreignField: '_id',
          as: 'stitches',
        },
      },
      { $unwind: '$stitches' },
      {
        $lookup: {
          from: 'translations',
          let: { stitch: '$stitches._id', language: language._id },
          pipeline: [
            {
              $match: { $expr: { $and: [{ $eq: ['$language', '$$language'] }, { $eq: ['$stitch', '$$stitch'] }] } },
            },
            { $project: { _id: 0, name: 1, application: 1, description: 1 } },
          ],
          as: 'translation',
        },
      },
      { $unwind: '$translation' },
      {
        $addFields: {
          'stitches.name': '$translation.name',
          'stitches.application': '$translation.application',
          'stitches.description': '$translation.description',
        },
      },
      {
        $project: {
          'stitches._id': 0,
          'stitches.createdAt': 0,
          'stitches.updatedAt': 0,
          translation: 0,
        },
      },
      {
        $sort: { 'stitches.index': 1 },
      },
      {
        $group: {
          _id: '$_id',
          name: { $first: '$name' },
          number: { $first: '$number' },
          stitches: { $push: '$stitches' },
        },
      },
    ]);
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

module.exports = (app) => {
  app.get('/api/pdf/:file', isAuthenticated, async (req, res) => {
    try {
      fs.readFile(path.resolve('public', 'pdf', req.params.file), (err, data) => {
        if (err) {
          console.log(err);
          return res.status(500).send(`Unable to open ${req.params.file}`);
        }

        res.contentType('application/pdf');
        res.send(data);
      });
    } catch (e) {
      return res.status(500).send({ error: e });
    }
  });

  app.get('/api/v2/pdf-generate', isAuthenticated, async (req, res) => {
    const { machine, lang } = req.query;
    if (!machine) {
      return res.status(500).send({ error: 'Missing machine parameter' });
    }

    if (!lang) {
      return res.status(500).send({ error: 'MIssing lang parameter' });
    }

    const language = await Language2.findOne({ name: lang });

    if (!language) {
      return res.status(500).json({ message: 'Unknown language' });
    }

    try {
      const data = await getMachine(machine, language);

      if (!data) {
        return res.status(500).json({ message: `There are no translations in ${language.name}` });
      }

      // ↓↓↓↓ UNCOMMENT FOR TESTING ↓↓↓↓
      // return res.status(200).send(data || { message: 'Something went wrong' });
      // ----------------------------------

      const file = await pdfgenerate(
        `SINGER-${data.number}-${language.name}.pdf`,
        data,
        language.name,
        language.nativeName
      );

      if (file.err) {
        await createLog(req, 'error_pdf_generate', { language: language.name, machine: data.number });

        return res.status(500).json({ error: file.err });
      }

      await Pdf.findOneAndUpdate(
        { machine: data._id, language: language._id },
        {
          $set: {
            machine: data._id,
            file: file.path,
            newVersion: false,
            language: language._id,
          },
        },
        { upsert: true, useFindAndModify: false }
      );

      await createLog(req, 'success_pdf_generate', { language: language.name, machine: data.number });

      return res.status(200).json({ file: file.path, name: data.number });
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error });
    }
  });

  app.get('/api/v2/pdf-multi-generate', isAuthenticated, async (req, res) => {
    req.setTimeout(360000);

    const { machine, group } = req.query;

    if (!machine) {
      return res.status(500).send({ error: 'Missing machine parameter' });
    }

    if (!group) {
      return res.status(500).send({ error: 'MIssing group parameter' });
    }

    const languageGroup = await LanguageGroup2.findOne({ name: group }).populate('languages');

    if (!languageGroup) {
      return res.status(500).json({ message: 'Unknown language group' });
    }

    const { number } = await Machine2.findById(machine).lean();

    try {
      const machines = [];
      for (const language of languageGroup.languages) {
        const machineData = await getMachine(machine, language);
        if (machineData) machines.push(machineData);
      }

      const file = await multiPDFGenerate(
        `SINGER-${number}-${languageGroup.name}.pdf`,
        machines,
        languageGroup.languages.map((a) => a.name),
        languageGroup.languages.map((a) => a.nativeName || a.name)
      );

      if (file.err) {
        await createLog(req, 'error_pdf_generate', { languageGroup: languageGroup.name, machine: number });

        return res.status(500).json({ error: file.err });
      }

      await Pdf.findOneAndUpdate(
        { machine: ObjectId(machine), languageGroup: languageGroup._id },
        {
          $set: {
            machine: ObjectId(machine),
            file: file.path,
            newVersion: false,
            languageGroup: languageGroup._id,
          },
        },
        { upsert: true, useFindAndModify: false }
      );

      await createLog(req, 'success_pdf_generate', { languageGroup: languageGroup.name, machine: number });

      return res.status(200).json({ file: file.path, name: number });
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error });
    }
  });
};
