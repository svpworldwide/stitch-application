const PdfPrinter = require('pdfmake');
const fs = require('fs');
const path = require('path');
const axios = require('axios');

const fonts = {
  Roboto: {
    normal: './utils/fonts/Roboto/Roboto-Regular.ttf',
    bold: './utils/fonts/Roboto/Roboto-Bold.ttf',
    italics: './utils/fonts/Roboto/Roboto-Italic.ttf',
  },
  Arabic: {
    normal: './utils/fonts/Arabic/MarkaziText-Regular.ttf',
    bold: './utils/fonts/Arabic/MarkaziText-Bold.ttf',
  },
  Chinese: {
    normal: './utils/fonts/Chinese/mingliu.TTF',
    bold: './utils/fonts/Chinese/mingliu.TTF',
  },
};

const setTopMarginOfCellForVerticalCentering = (ri, node) => {
  const calcCellHeight = (cell, ci) => {
    if (cell._height !== undefined) {
      return cell._height;
    }
    let width = 0;
    for (let i = ci; i < ci + (cell.colSpan || 1); i++) {
      width += node.table.widths[i]._calcWidth;
    }
    let calcLines = (inlines) => {
      let tmpWidth = width;
      let lines = 1;
      inlines.forEach((inline) => {
        tmpWidth = tmpWidth - inline.width;
        if (tmpWidth < 0) {
          lines++;
          tmpWidth = width - inline.width;
        }
      });
      return lines;
    };

    cell._height = 0;
    if (cell._inlines && cell._inlines.length) {
      let lines = calcLines(cell._inlines);
      cell._height = cell._inlines[0].height * lines;
    } else if (cell.stack && cell.stack[0] && cell.stack[0]._inlines[0]) {
      cell._height = cell.stack
        .map((item) => {
          let lines = calcLines(item._inlines);
          return item._inlines[0].height * lines;
        })
        .reduce((prev, next) => prev + next);
    } else if (cell.table) {
      // TODO...
      console.log(cell);
    }

    cell._space = cell._height;
    if (cell.rowSpan) {
      for (let i = ri + 1; i < ri + (cell.rowSpan || 1); i++) {
        cell._space += Math.max(...calcAllCellHeights(i)) + padding * (i - ri) * 2;
      }
      return 0;
    }

    ci++;
    return cell._height;
  };
  const calcAllCellHeights = (rIndex) => {
    return node.table.body[rIndex].map((cell, ci) => {
      return calcCellHeight(cell, ci);
    });
  };

  calcAllCellHeights(ri);
  const maxRowHeights = {};
  node.table.body[ri].forEach((cell) => {
    if (!maxRowHeights[cell.rowSpan] || maxRowHeights[cell.rowSpan] < cell._space) {
      maxRowHeights[cell.rowSpan] = cell._space;
    }
  });

  node.table.body[ri].forEach((cell) => {
    if (cell.ignored) return;

    if (cell._rowSpanCurrentOffset) {
      cell._margin = [0, 0, 0, 0];
    } else {
      let topMargin = (maxRowHeights[cell.rowSpan] - cell._height) / 2;
      if (cell._margin) {
        cell._margin[1] += topMargin;
      } else {
        cell._margin = [0, topMargin, 0, 0];
      }
    }
  });

  return 2;
};

const printer = new PdfPrinter(fonts);

const getImage = async (url) => {
  let image = '',
    imageResponse;
  try {
    imageResponse = await axios.get(url, { responseType: 'arraybuffer' });
  } catch (err) {
    throw new Error(err.message);
  }
  image = new Buffer.from(imageResponse.data);

  return image;
};

const multiPDFGenerate = async (fileName, machines, languages, nativeLanguages) => {
  const file = path.resolve('public', 'pdf', fileName);

  let content = [
    // { text: `${machines[0].name.toUpperCase()}${String.fromCharCode(174)} ${machines[0].number}`, style: 'header' },
    {
      columns: [
        { text: machines[0].name.toUpperCase(), style: 'header', alignment: 'left', width: 'auto' },
        { text: String.fromCharCode(174), fontSize: 14, width: 'auto' },
        { text: '  ', style: 'header', width: 8 },
        { text: machines[0].number, style: 'header', alignment: 'left', width: 'auto' },
      ],
    },
    { text: nativeLanguages.join(' | '), style: 'subheader' },
  ];

  for (let i = 0; i <= languages.length - 1; i++) {
    let font = 'Roboto';
    if (languages[i] === 'Arabic' || languages[i] === 'Chinese') font = languages[i];
    let stitchesForTable = [[{ text: nativeLanguages[i], colSpan: 5, style: 'tableHeader' }, {}, {}, {}, {}]];
    for (let index = 0; index <= machines[i].stitches.length - 1; index++) {
      let image = '';
      if (machines[i].stitches[index].image) {
        image = await getImage(machines[i].stitches[index].image);
        stitchesForTable.push([
          { text: index + 1, font: 'Roboto' },
          { text: machines[i].stitches[index].name, font: font },
          { text: machines[i].stitches[index].application, font: font },
          {
            image: image,
            fit: [30, 55],
          },
          { text: machines[i].stitches[index].description, font: font },
        ]);
      } else {
        stitchesForTable.push([
          { text: index + 1, font: 'Roboto' },
          { text: machines[i].stitches[index].name, font: font },
          { text: machines[i].stitches[index].application, font: font },
          {
            text: '',
            width: 30,
          },
          { text: machines[i].stitches[index].description, font: font },
        ]);
      }
    }
    if (i === 0) {
      content.push({
        style: 'table',
        layout: { paddingTop: setTopMarginOfCellForVerticalCentering },
        table: {
          headerRows: 1,
          dontBreakRows: true,
          body: stitchesForTable,
        },
      });
    } else {
      content.push({
        style: 'table',
        layout: { paddingTop: setTopMarginOfCellForVerticalCentering },
        pageBreak: 'before',
        table: {
          headerRows: 1,
          dontBreakRows: true,
          body: stitchesForTable,
        },
      });
    }
  }

  content.push([
    {
      text: 'SINGER, the Cameo “S” Design, and SINGER is SEWING MADE EASY are exclusive trademarks of The Singer Company Limited S.à r.l., or its Affiliates. ©2020 The Singer Company Limited S.à r.l. or its Affiliates. All rights reserved.',
      style: 'footer',
    },
    {
      text: 'Each model varies in its functionality and features. The descriptions for some applications may reference techniques or accessories that are not available or included with your machine model.',
      style: 'footer',
    },
  ]);

  const docDefinition = {
    content: content,
    footer: (currentPage, pageCount) => {
      return {
        text: currentPage.toString() + ' of ' + pageCount,
        fontSize: 10,
        alignment: 'center',
      };
    },
    styles: {
      header: {
        fontSize: 22,
        bold: true,
        margin: [0, 0, 0, 5],
      },
      subheader: {
        fontSize: 18,
        alignment: 'left',
        bold: true,
        margin: [0, 5, 0, 15],
      },
      table: {
        fontSize: 11,
      },
      tableHeader: {
        fontSize: 14,
        bold: true,
        alignment: 'left',
        margin: [0, 5, 0, 5],
        color: 'black',
        font: 'Roboto',
      },
      footer: {
        fontSize: 6,
        margin: [0, 5, 0, 0],
        color: 'black',
      },
    },
  };

  try {
    let doc = printer.createPdfKitDocument(docDefinition);
    doc.pipe(fs.createWriteStream(file)); // write to PDF
    doc.end();
    return { path: '/pdf/' + fileName };
  } catch (e) {
    console.log(e);
    return { err: e.message };
  }
};

module.exports = multiPDFGenerate;
