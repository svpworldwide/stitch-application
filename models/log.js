const mongoose = require('mongoose');

const enums = [
  'log_in',
  'log_out',
  'register',
  'delete_user',
  'update_user',
  'create_language',
  'update_language',
  'delete_language',
  'create_language_group',
  'update_language_group',
  'delete_language_group',
  'create_machine',
  'update_machine',
  'delete_machine',
  'create_translation',
  'update_translation',
  'delete_translation',
  'account_verification',
  'forgot_password',
  'reset_password',
  'success_pdf_generate',
  'error_pdf_generate',
  'attempt_save_2db',
  'success_save_2db',
  'error_save_2db',
  'group_create_success',
  'group_create_error',
  'update_stitch_success',
  'update_stitch_error',
];

const logSchema = new mongoose.Schema(
  {
    username: { type: String, trim: true },
    email: { type: String, trim: true },
    ip: { type: String, trim: true },
    action: {
      type: String,
      enum: enums,
    },
    user: { type: String },
    language: { type: String },
    languageGroup: { type: String },
    machine: { type: String },
    stitchesCount: { type: Number },
    stitch: { type: String },
    file: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Log', logSchema);
