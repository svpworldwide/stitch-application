const mongoose = require('mongoose');

const machineSchema2 = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true },
    number: { type: String, required: true, trim: true, index: true },
    stitches: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Stitch2' }],
  },
  { timestamps: true }
);

const Machine2 = mongoose.model('Machine2', machineSchema2, 'machines');

exports.Machine2 = Machine2;
