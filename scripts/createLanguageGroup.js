const mongoose = require('mongoose');
const Log = require('../models/log').Log;
const Language = require('../models/language').Language;
const LanguageGroup = require('../models/languageGroup').LanguageGroup;
require('dotenv').config();

const groupName = process.argv[2];
const languagesArg = process.argv[3];
if ([undefined, '', ' ', '?', '-?', '-h', '--help'].includes(groupName)) {
  console.log('\nUsage: node createLanguageGroup.js <group-name> <comma-separated-list-of-languages>');
  console.log('\nThere should be NO spaces in list of languages!');
  console.log('\nExample: node createLanguageGroup.js NA English,Spanish,French\n');
  return;
}
const languages = languagesArg.split(',');
console.log('\nname = ', groupName);
console.log('\nlanguages = ', languages);
console.log('\n');

mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('\nDatabase connected...\n');
    const existingLanguages = await Language.find({ language: { $in: languages } }).exec();
    if (existingLanguages.length !== languages.length) throw new Error('One or more languages NOT FOUND in database!');
    // await LanguageGroup.deleteMany({}).exec();
    const english = await Language.findOne({ language: 'English' }).exec();
    const machines = english.machines.map((mach) => ({
      name: mach.name,
      number: mach.number,
      stitches: mach.stitches.length,
      stitchIndexes: mach.stitches.map((s) => s.index),
    }));
    const group = await new LanguageGroup({
      name: groupName,
      languages: languages,
      machines: machines,
    }).save();
    if (group && group.name === groupName) {
      await new Log({
        username: 'admin',
        ip: '0.0.0.0',
        action: 'group_create_success',
      }).save();
      console.log('\nResult: ', 'SAVED');
    }
  })
  .catch(async (e) => {
    console.log('\nError:');
    console.log(e.message);
    console.log('\nResult: ', 'NOT SAVED');
    await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'group_create_error',
    }).save();
  })
  .finally(() => {
    mongoose.disconnect();
    console.log('\nDatabase disconnected\n');
  });
