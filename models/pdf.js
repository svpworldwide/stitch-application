const mongoose = require('mongoose');

const pdfSchema = new mongoose.Schema(
  {
    machine: { type: mongoose.Schema.Types.ObjectId, ref: 'Machine2', required: true, index: true },
    file: { type: String, trim: true, required: true },
    newVersion: { type: Boolean },
    language: { type: mongoose.Schema.Types.ObjectId, ref: 'Language2' },
    languageGroup: { type: mongoose.Schema.Types.ObjectId, ref: 'LanguageGroup2' },
  },
  { timestamps: true }
);

const Pdf = mongoose.model('Pdf', pdfSchema, 'pdf_files');

exports.Pdf = Pdf;
