const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    firstName: { type: String, trim: true },
    lastName: { type: String, trim: true },
    username: {
      type: String,
      trim: true,
      unique: true,
    },
    role: { type: String, enum: ['admin', 'superadmin'], required: true },
    status: { type: String, enum: ['active', 'blocked', 'verification'], required: true },
    email: { type: String, unique: true, required: true, lowercase: true, trim: true },
    password: { type: String, required: true },
    token: { type: String, trim: true },
    passwordToken: { type: String, trim: true },
  },
  { timestamps: true }
);

const User = mongoose.model('User', userSchema);

module.exports = User;
