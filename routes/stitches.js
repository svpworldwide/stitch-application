const { Stitch2 } = require('../models/stitch');
const { isAuthenticated } = require('./authMiddleware');

module.exports = (app) => {
  app.get('/api/v2/stitches', isAuthenticated, async (req, res) => {
    try {
      const data = await Stitch2.find({}).sort({ index: 1 }).exec();
      return res.status(200).json(data);
    } catch (e) {
      return res.status(500).json({ error: e });
    }
  });
};
