module.exports = {
  apps: [
    {
      name: 'singer-api',
      script: './index.js',
      port: 3400,
      env_production: {
        NODE_ENV: 'production',
        PORT: 3400,
      },
      time: true,
    },
    {
      name: 'singer-dashboard',
      script: 'cd client && npm run start:prod',
      port: 3402,
      env_production: {
        NODE_ENV: 'production',
        PORT: 3402,
      },
      time: true,
    },
  ],
  deploy: {
    staging: {
      user: 'dev',
      host: '37.120.187.75',
      key: './key/concordtest.pem',
      ref: 'origin/staging',
      repo: 'https://djomla024@bitbucket.org/djomla024/singer.git',
      path: '/mnt/datapart/singer-pdf-generator',
      'post-deploy':
        'cd client && npm i && npm run build && cd .. && npm i && pm2 startOrRestart ecosystem.config.js --env production',
    },
    production: {
      user: 'ec2-user',
      host: 'ec2-13-59-91-137.us-east-2.compute.amazonaws.com',
      key: './key/stitch-app.pem',
      ref: 'origin/master',
      repo: 'https://djomla024@bitbucket.org/djomla024/singer.git',
      path: '/home/ec2-user/stitch-app',
      'post-deploy':
        'cd client && npm i && npm run build && cd .. && npm i && pm2 startOrRestart ecosystem.config.js --env production',
    },
  },
};
