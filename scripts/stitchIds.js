const { readFile, utils } = require('xlsx');
const mongoose = require('mongoose');
const { Log } = require('../models/log');
const { StitchId } = require('../models/stitchIds');
require('dotenv').config();

const arg = process.argv[2];
let workbook;
if ([undefined, '', ' ', '?', '-?', '-h', '--help'].includes(arg)) {
  console.log('\nUsage: node stitchIds.js <path-to-file>');
  console.log('\nExample: node stitchIds.js ..doc/Stitch_Applications_Sewing_Worksheet_Translations_LINKS.xlsx\n');
  return;
} else {
  try {
    workbook = readFile(arg);
  } catch (e) {
    console.log(`\n${arg} is not a valid xlsx file!\n`);
    return;
  }
}

mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('\nDatabase connected...\n');
    const json = createStitchIds();
    const save = await new StitchId({ stitchIds: json }).save();
    await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'success_save_2db',
    }).save();
    console.log('\nResult: ', save);
  })
  .catch(async (e) => {
    console.log('\nError saving to database!');
    console.log(e.message);
    console.log('\nResult: ', 'NOT SAVED');
    await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'error_save_2db',
    }).save();
  })
  .finally(() => {
    mongoose.disconnect();
    console.log('\nDatabase disconnected\n');
  });

const createStitchIds = () => {
  let stitches = [];
  const sheet = workbook.Sheets['English'];
  const stitchesRange = 'A5:A785';
  sheet['!ref'] = stitchesRange;
  const range = utils.decode_range(sheet['!ref']);
  for (row = range.s.r; row <= range.e.r; row++) {
    let stitch = {};
    const currCell = sheet[utils.encode_cell({ r: row, c: 0 })];
    stitch.appNumber = currCell.w;
    stitch.index = row - 4;
    stitches.push(stitch);
  }
  console.log('\nstitches length = ', stitches.length);
  return stitches;
};
