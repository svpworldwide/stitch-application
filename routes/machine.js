const fs = require('fs');
const path = require('path');
const { isAuthenticated } = require('./authMiddleware');
const createLog = require('../utils/createLog');
const { Language2 } = require('../models/language');
const { Machine2 } = require('../models/machine');
const { LanguageGroup2 } = require('../models/languageGroup');
const { Pdf } = require('../models/pdf');

module.exports = (app) => {
  app.get('/api/v2/machines', isAuthenticated, async (req, res) => {
    try {
      const data = await Machine2.find().exec();
      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.post('/api/v2/machines', isAuthenticated, async (req, res) => {
    try {
      const checkData = await Machine2.find({ number: req.body.number }).lean();

      if (checkData && checkData.length > 0) throw new Error(`Machine ${req.body.number} already exists!`);

      const data = await new Machine2(req.body).save();

      await createLog(req, 'create_machine', {
        machine: req.body.number,
        stitchesCount: req.body.stitches.length,
      });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error.message);
      return res.status(500).json({ message: error.message });
    }
  });

  app.put('/api/v2/machines/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const data = await Machine2.findOneAndUpdate({ _id: id }, req.body, { useFindAndModify: false });

      await createLog(req, 'update_machine', {
        machine: req.body.number,
        stitchesCount: req.body.stitches.length,
      });

      await Pdf.updateMany({ machine: id }, { $set: { newVersion: true } });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.delete('/api/v2/machines/:id', isAuthenticated, async (req, res) => {
    const { id } = req.params;
    if (!id || id.length !== 24) {
      return res.status(401).json({ message: 'Wrong id format.' });
    }

    try {
      const files = await Pdf.find({ machine: id }).exec();
      for (const { _id, file } of files) {
        const filePath = path.resolve('public', 'pdf', file.replace('/pdf/', ''));
        if (fs.existsSync(filePath)) {
          fs.unlinkSync(filePath);
        }
        await Pdf.findOneAndDelete({ _id });
      }

      const data = await Machine2.findOneAndDelete({ _id: id });

      await createLog(req, 'delete_machine', {
        machine: data.number,
      });

      return res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });

  app.get('/api/v2/machines/:lang', isAuthenticated, async (req, res) => {
    const { lang } = req.params;
    if (!lang) {
      return res.status(500).json({ message: 'Missing language parameter' });
    }

    try {
      let language = await Language2.findOne({ name: lang });
      const matchExprAnd = [{ $eq: ['$machine', '$$machineId'] }, { $eq: ['$language', '$$languageId'] }];
      if (!language) {
        language = await LanguageGroup2.findOne({ name: lang }).populate('languages');
        matchExprAnd[1] = { $eq: ['$languageGroup', '$$languageId'] };
      }

      if (!language) {
        return res.status(500).json({ message: 'Unknown language' });
      }

      const data = await Machine2.aggregate([
        { $match: {} },
        { $addFields: { stitches: { $size: '$stitches' } } },
        {
          $lookup: {
            from: 'pdf_files',
            let: { machineId: '$_id', languageId: language._id },
            pipeline: [
              {
                $match: {
                  $expr: { $and: matchExprAnd },
                },
              },
              { $project: { _id: 0, file: 1, newVersion: 1 } },
            ],
            as: 'pdf',
          },
        },
        { $unwind: { path: '$pdf', preserveNullAndEmptyArrays: true } },
        { $addFields: { pdfFile: '$pdf.file', newVersion: '$pdf.newVersion' } },
        { $project: { pdf: 0 } },
        // { $sort: { number: 1 } },
      ]);

      const languages = language.languages && language.languages.map((lang) => lang.name);

      return res.status(200).json({ language: language.name, languages: languages || null, machines: data });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  });
};
