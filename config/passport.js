const passport = require('passport');
const { Strategy } = require('passport-local');
const User = require('../models/user');

const verifyCallback = async (username, password, done) => {
  try {
    const user = await User.findOne({ $or: [{ username: username }, { email: username }] }).exec();

    if (!user) {
      return done(null, false);
    }

    if (user.password === password && user.status === 'active') {
      return done(null, user);
    } else {
      return done(null, false);
    }
  } catch (error) {
    done(error);
  }
};

const strategy = new Strategy(verifyCallback);

passport.use(strategy);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((userId, done) => {
  User.findById(userId)
    .then((user) => {
      done(null, user);
    })
    .catch((err) => done(err));
});
