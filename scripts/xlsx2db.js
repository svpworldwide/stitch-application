const { readFile, utils } = require('xlsx');
const mongoose = require('mongoose');
const Log = require('../models/log').Log;
const Language = require('../models/language').Language;
require('dotenv').config();

const arg = process.argv[2];
let workbook;
if ([undefined, '', ' ', '?', '-?', '-h', '--help'].includes(arg)) {
  console.log('\nUsage: node xlsx2db.js <path-to-file>');
  console.log('\nExample: node xlsx2db.js ..doc/Stitch_Applications_Sewing_Worksheet_Translations_LINKS.xlsx\n');
  return;
} else {
  try {
    workbook = readFile(arg);
  } catch (e) {
    console.log(`\n${arg} is not a valid xlsx file!\n`);
    return;
  }
}

mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('\nDatabase connected...\n');
    await Language.deleteMany({}).exec();
    const save = await workbook2db();
    await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'success_save_2db',
    }).save();
    console.log('\nResult: ', save);
  })
  .catch(async (e) => {
    console.log('\nError saving to database!');
    console.log(e.message);
    console.log('\nResult: ', 'NOT SAVED');
    await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'error_save_2db',
    }).save();
  })
  .finally(() => {
    mongoose.disconnect();
    console.log('\nDatabase disconnected\n');
  });

const sheet2json = (sheet, sheetName, machinesDataRange) => {
  let json = { language: sheetName };
  let machines = [];
  sheet['!ref'] = machinesDataRange;
  const range = utils.decode_range(sheet['!ref']);
  for (col = range.s.c; col <= range.e.c; col++) {
    let machine = {};
    machine.name = 'Singer';
    machine.number = sheet[utils.encode_cell({ r: 1, c: col })].w;
    machine.pdf = '';
    machine.stitches = [];
    for (row = range.s.r; row <= range.e.r; row++) {
      const currCell = sheet[utils.encode_cell({ r: row, c: col })];
      if (currCell && currCell.v === 1) {
        // if machine supports stitch save it to array
        let stitch = {};
        stitch.index = row - 4;
        const appNumber = sheet[utils.encode_cell({ r: row, c: 0 })]; // col A is app number
        stitch.appNumber = appNumber && appNumber.w ? appNumber.w : '';
        const name = sheet[utils.encode_cell({ r: row, c: 1 })]; // col B is name
        stitch.name = name && name.w ? name.w : '';
        const image = sheet[utils.encode_cell({ r: row, c: 2 })]; // col C is image
        stitch.image = image && image.w ? image.w : '';
        const application = sheet[utils.encode_cell({ r: row, c: 3 })]; // col D is application
        stitch.application = application && application.w ? application.w : '';
        const description = sheet[utils.encode_cell({ r: row, c: 5 })]; // col F is description
        stitch.description = description && description.w ? description.w : 'No description avaialble';
        machine.stitches.push(stitch);
      }
    }
    machines.push(machine); // save machine to array
  }
  json.machines = machines; // save all machines to language
  return json;
};

const json2db = async (json) => {
  try {
    const data = await new Language(json).save();
    const log = await new Log({
      username: 'admin',
      ip: '0.0.0.0',
      action: 'success_save_2db',
    }).save();
    return { data, log };
  } catch (e) {
    console.log('\nError converting to json!\n');
    console.log(e);
    return { data, log: { action: 'error_save_2db' } };
  }
};

const workbook2db = async () => {
  let result;
  for (let sheet of workbook.SheetNames) {
    const worksheet = workbook.Sheets[sheet];
    const json = sheet2json(worksheet, sheet, 'O5:BM785'); // machine data range
    result = await json2db(json)
      .then((saved) => {
        console.log('worksheet: ', saved.data.language);
        console.log('action: ', saved.log.action);
        return 'SAVED';
      })
      .catch((e) => {
        console.log('\nNOT saved!');
        console.log(e);
        return 'NOT SAVED';
      });
  }
  return result;
};
