const Log = require('../models/log');

const createLog = async (req, action, data = {}) => {
  return await new Log({
    username: req.user ? req.user.username : 'NN',
    email: req.user ? req.user.email : 'NN',
    ip: req.ip || 'NN',
    action,
    ...data,
  }).save();
};

module.exports = createLog;
