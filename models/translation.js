const mongoose = require('mongoose');

const translationSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true },
    application: { type: String, required: true, trim: true },
    description: { type: String, required: true, trim: true },
    language: { type: mongoose.Schema.Types.ObjectId, ref: 'Language2', required: true, index: true },
    stitch: { type: mongoose.Schema.Types.ObjectId, ref: 'Stitch2', required: true, index: true },
  },
  { timestamps: true }
);

const Translation = mongoose.model('Translation', translationSchema);

exports.Translation = Translation;
