const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).json({ message: 'Not authenticated' });
  }
};

const isAuthorized =
  (roles = ['admin']) =>
  (req, res, next) => {
    const { user } = req;

    if (user && roles.includes(user.role)) {
      next();
    } else {
      res.status(403).json({ message: `Forbidden for ${user.role}` });
    }
  };

module.exports = { isAuthenticated, isAuthorized };
