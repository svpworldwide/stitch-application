const { isAuthenticated, isAuthorized } = require('./authMiddleware');
const createLog = require('../utils/createLog');
const User = require('../models/user');

module.exports = (app) => {
  // get all users
  app.get('/api/users', isAuthenticated, isAuthorized(['superadmin']), async (req, res) => {
    const users = await User.find({}).select('-password').sort({ email: 1 });

    return res.status(200).json(users);
  });

  // get user by id
  app.get('/api/user/:id', isAuthenticated, isAuthorized(['superadmin']), async (req, res) => {
    const user = await User.findById(req.params.id).select('-password');

    return res.status(200).json(user);
  });

  // update user by id
  app.put('/api/user/:id', isAuthenticated, isAuthorized(['superadmin']), async (req, res) => {
    const user = await User.findOneAndUpdate({ _id: req.params.id }, { $set: req.body }, { useFindAndModify: false });

    await createLog(req, 'update_user', { user: user.email });

    return res.status(200).json(user);
  });

  // delete user by id
  app.delete('/api/user/:id', isAuthenticated, isAuthorized(['superadmin']), async (req, res) => {
    const user = await User.findOneAndDelete({ _id: req.params.id });

    await createLog(req, 'delete_user', { user: user.email });

    return res.status(200).json(user);
  });
};
